# Training on CVLHDS

```
python Code/ctc_main.py --exp_name=CVLHDS --dataset=cvlhds
```

# Training on CVLHDS + ORAND

```
python Code/ctc_main.py --exp_name=CVLHDS_ORAND  --dataset=orand --model_path=./checkpoints/CVLHDS/models/model.pt
```

# Eval

```
python Code/ctc_main.py --exp_name=CVLHDS_ORAND_eval --eval --dataset=orand --model_path=./checkpoints/CVLHDS_ORAND/models/model.pt
```
