# Training on MNIST

```
python Code/ctc_main.py --exp_name=MNIST --epochs=2000 --dataset=mnist
```

# Training on MNIST + ORAND

```
python Code/ctc_main.py --exp_name=MNIST_ORAND  --dataset=orand --model_path=./checkpoints/MNIST/models/model.pt
```

# Eval

```
python Code/ctc_main.py --exp_name=MNIST_ORAND_eval --eval --dataset=orand --model_path=./checkpoints/MNIST_ORAND/models/model.pt
```
