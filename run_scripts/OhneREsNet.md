# Ohne ResNet

# Training on ORAND

```
python Code/ctc_main.py --exp_name=ORAND_NO_RESNET --encoder_model=cnn --dataset=orand
```

# Eval

```
python Code/ctc_main.py --exp_name=ORAND_NO_RESNET_eval --encoder_model=cnn --eval --dataset=orand --model_path=./checkpoints/ORAND_NO_RESNET/models/model.pt
```
