# Training on ORAND

```
python Code/ctc_main.py --exp_name=ORAND --dataset=orand
```

# Eval

```
python Code/ctc_main.py --exp_name=ORAND_eval --eval --dataset=orand --model_path=./checkpoints/ORAND/models/model.pt
```
