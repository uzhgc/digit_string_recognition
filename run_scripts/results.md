# Wrong: Trained only on subnet

# No ResNet

Evaluation Result: levenshtein: 0.7671 acc: 0.0000 acc10: 0.0037 accSoft: 0.1264

# Baseline

Evaluation Result: levenshtein: 0.0272 acc: 0.8937 acc10: 0.9645 accSoft: 0.9612

# CVLHD

Evaluation Result: levenshtein: 0.0282 acc: 0.8881 acc10: 0.9595 accSoft: 0.9573

# MNIST

Evaluation Result: levenshtein: 0.0296 acc: 0.8817 acc10: 0.9598 accSoft: 0.9584
