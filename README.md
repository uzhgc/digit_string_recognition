# digit_string_recognition

python version: 3.11.5
pip version:    23.3.2


## Create Virtual Env

Windows
  + python -m venv env
  + .\env\Scripts\activate

UNIX
  + python3 -m venv env
  + source env/bin/activate

# Install Dependencies
+ run 'pip install -r requirements.txt'

# Run CTC Approach

+ Show options
  + python src/ctc_main.py --help
+ Example Configurations
  + see ./run_scripts/

Notice: For the first run datasets are downloaded which can be time-consuming.

# Custom LSTM
+ The self implementation of the LSTM can be found:
  + ./src/models/CustomLSTM


# Results
![Model Performance Results](doc/images/results.png "Model Performance Results")

