# Connectionist Temporal Classification

- Issues without ctc:
	- model does not know where the images are located
	- model does not know to read the image from left to right

- Neural network architecture:
	- CNN --> extract a sequence of features
	- RNN --> predict a digit for each element of the feature sequence
	- output: digit scores for each sequence element (output matrix)

- approach:
	![[Bildschirmfoto 2024-01-18 um 18.02.19.png]]

- CTC Loss:
	- we do not need to annotate each horizontal position --> ctc loss
	- calculates the loss of all possible alignments of the ground truth and takes the sum of all scores
	- it does not matter where the text appears in the image
- Encoding the text
	- issue: how to encode duplicate digits? --> blank symbol
	- examples:
		- 123 --> 11122233 or  12222333
		- 100 --> 11__0_00 or 111000_00 but **not** 11100000
	- we can create different alignments of the same text
-  Training:
	- goal: maximize the probability of a correct classification <==> minimize the NLL-Loss
	- for each path, multiply the digit probabilities at each time step to get the paths probability
	- sum up the probabilities of paths that lead to the correct label
- Inference:
	- calculate the most likely digit sequence given the output matrix of the model
	- we cannot compute the probabilities of every possible digit string because this is too computational expensive
	- greedy decoding:
		1. take the most likely digit at each time step
		2. undo the encoding
			1. remove duplicate digits
			2. remove all blank symbols
			

