import torch
from torch import nn
from models.custom_lstm import CustomLSTM

class DigitClassifier(nn.Module):

    def __init__(self, encoder_model="cnn"):
        super(DigitClassifier, self).__init__()
        hidden_size = 256
        self.freeze_encoder = False
        if encoder_model == "cnn":
            dim = 288
            self.embed = nn.Sequential(
                nn.Conv2d(3, 16, kernel_size=3, stride=(2, 1), padding=(1, 1), bias=False),
                nn.ELU(),
                nn.Conv2d(16, 32, kernel_size=3, stride=(1, 1), padding=(1, 1), bias=False),
                nn.ELU(),
                nn.Conv2d(32, 64, kernel_size=3, stride=(2, 2), padding=(1, 1), bias=False),
                nn.ELU(),
                nn.Conv2d(64, 32, kernel_size=3, stride=(1, 1), padding=(1, 1), bias=False),
                nn.ELU()
            )
        elif encoder_model == "resnet":
            self.freeze_encoder = True
            dim = 8192
            RESNET_DEPTH = 18
            REPO = 'pytorch/vision'
            resnet = torch.hub.load(REPO, f'resnet{RESNET_DEPTH}', weights=f'ResNet{RESNET_DEPTH}_Weights.DEFAULT')
            resnet = nn.Sequential(*list(resnet.children())[:-2])
            self.embed = resnet

        self.rnns = nn.ModuleList()
        self.rnns.append(CustomLSTM(input_size=dim, hidden_size=hidden_size))
        self.output_layer = nn.Linear(in_features=hidden_size, out_features=10 + 1)

    def forward(self, features):
        if self.freeze_encoder:
            self.embed.eval()
        
        embedding = self.embed(features)
        n, c, h, w = embedding.size()
        embedding = embedding.view(n, c*h, w).permute(2, 0, 1)
        h = embedding
        for l in self.rnns:
            h, _ = l(h)
        logits = self.output_layer(h)
        lengths = torch.zeros((n,)).fill_(w)
        return logits, lengths
