import torch
import torch.nn as nn
import torchvision.models as models
import numpy as np
from tensorflow.keras.utils import to_categorical
from custom_lstm import CustomLSTMCell

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")


class Encoder(nn.Module):
    def __init__(self, resnet_model, hidden_size):
        super(Encoder, self).__init__()

        # Freeze ResNet parameters
        # for param in resnet_model.parameters():
        #    param.requires_grad = False
        ## TODO: Fehler? neues PRetrained?
        print("CNN")
        self.features = nn.Sequential(
            nn.Conv2d(3, 16, kernel_size=3, stride=1, padding=1),
            nn.ReLU(),
            nn.MaxPool2d(kernel_size=2, stride=2),
            
            nn.Conv2d(16, 8, kernel_size=3, stride=1, padding=1),
            nn.ReLU(),
            nn.MaxPool2d(kernel_size=2, stride=2),
            
            nn.Conv2d(8, 4, kernel_size=3, stride=1, padding=1),
            nn.ReLU(),
            nn.MaxPool2d(kernel_size=2, stride=2)
        )
        
        self.classifier = nn.Sequential(
            nn.Linear( 4* 32 * 32, hidden_size),
            nn.ReLU()
        )
        #self.resnet = nn.Sequential(*list(resnet_model.children())[:-1])
        ######print(f"Encoder Arc: {resnet_model.fc.in_features} zu {hidden_size}")
        #self.fc = nn.Linear(resnet_model.fc.in_features, hidden_size)

    def forward(self, input_seq):
        #resnet_output = self.resnet(input_seq)
        ###print(f"Encoder shape resnet before: {resnet_output.shape}")
        #esnet_output = resnet_output.view(resnet_output.size(0), -1)
        ###print(f"Encoder shape resnet after: {resnet_output.shape}")
        x = self.features(input_seq)

        x = x.view(x.size(0), -1)
        encoded = self.classifier(x)
        #encoded = self.fc(resnet_output)
        return encoded


class Decoder(nn.Module):
    def __init__(
        self,
        hidden_size,
        teacher_force_ratio,
        MAX_LEN,
        max_epoch,
        CUSTOMLSTMCELL,
        output_size=11,
        num_layers=1,
        p_dropout=0.2,
    ):
        super(Decoder, self).__init__()
        self.hidden_size = hidden_size
        self.output_size = output_size
        self.num_layers = num_layers
        self.p_dropout = p_dropout
        if CUSTOMLSTMCELL:
            self.lstm = CustomLSTMCell(input_size=output_size, hidden_size=hidden_size)
        else:
            self.lstm = nn.LSTMCell(input_size=output_size, hidden_size=hidden_size)
        self.fc = nn.Linear(hidden_size, output_size)
        self.softmax = nn.Softmax()
        self.max_epoch = max_epoch
        self.teacher_force_ratio = teacher_force_ratio
        self.MAX_LEN = MAX_LEN

    def forward(self, source, target = None, epoch = None):
        # batch size
        batch_size = source.size(0)

        # init the hidden and cell states to zeros
        # TODO: other init?
        cell_state = torch.zeros((batch_size, self.hidden_size), device=source.device)
        assert source.shape == cell_state.shape

        if target is not None:
            # define the output tensor placeholder
            target = target.float()
            outputs = self.train_cell_1(batch_size, source, cell_state, target, epoch)
        else:
            outputs = self.infer_cell(batch_size, source, cell_state)

        return outputs

    def encode_labels(self, target, num_tokens=11):
        # Define the sequence length, number of tokens, and batch size
        sequence_length = target.shape[1]
        batch_size = target.shape[0]

        # Create a tensor of shape (sequence_length, num_tokens)
        one_hot_matrix = torch.zeros(
            batch_size, sequence_length, num_tokens, device=target.device
        )

        # Convert the original list to a tensor
        indices = target.clone().detach().long()
        # Use indexing to set the values to 1 at the specified indices
        one_hot_matrix.scatter_(2, indices.unsqueeze(2), 1)
        return one_hot_matrix

    def train_cell_1(self, batch_size, source, cell_state, target, epoch):
        # define the output tensor placeholder
        outputs = torch.empty(
            (batch_size, self.MAX_LEN, self.output_size), device=device
        )
        start = torch.ones((batch_size, self.output_size), device=device)

        # define end of sequence token
        eos = torch.zeros(self.output_size, device=device)
        eos[self.output_size - 1] = 1
        # out = torch.zeros(self.output_size)
        count = 0

        
        # one-hot encode vector [2,2], into [[0,1], [0,1]]
        one_hot_target = self.encode_labels(target)

        while count < self.MAX_LEN:
            if count == 0:
                # start with source and start token
                hidden_state, cell_state = self.lstm(start, (source, cell_state))
                # set count to != 0
            else:
                if epoch < self.max_epoch:
                    # for small epoch use target as input for faster convergence
                    input_data = one_hot_target[:, count - 1, :]
                else:
                
                    # determine whether to use teacher forcing or not based on the ratio
                    use_teacher_forcing = (
                        torch.rand(1).item() < self.teacher_force_ratio
                    )
                    # if using teacher forcing, input the real target; otherwise, use predictions

                    # TODO: Stimmt target -> eher one_hot_target, stimmt out?
                    input_data = (
                        one_hot_target[:, count - 1, :] if use_teacher_forcing else out
                    )


                hidden_state, cell_state = self.lstm(
                    input_data, (hidden_state, cell_state)
                )

            out = self.fc(hidden_state)
            out = self.softmax(out)

            outputs[:, count, :] = out

            # if count == 3:
            #    print(f"Decoder out prob: {out[:4]}")
            #    print(f"Decoder out argmax: {out[:4].argmax(dim = 1)}")
            #    print(f"Decoder output tensor: {outputs[:8, 3, :4]}")

            out = out.argmax(dim=1)

            # Perform one-hot encoding without a loop
            out = to_categorical(out.cpu().numpy(), num_classes=self.output_size)
            out = torch.tensor(out, device=device)

            count += 1

        return outputs
    
    def infer_cell(self, batch_size, source, cell_state):
        # define the output tensor placeholder
        outputs = torch.empty(
            (batch_size, self.MAX_LEN, self.output_size), device=device
        )
        start = torch.ones((batch_size, self.output_size), device=device)

        # define end of sequence token
        eos = torch.zeros(self.output_size, device=device)
        eos[self.output_size - 1] = 1
        # out = torch.zeros(self.output_size)
        count = 0

        while count < self.MAX_LEN:
            if count == 0:
                # start with source and start token
                hidden_state, cell_state = self.lstm(start, (source, cell_state))
                # set count to != 0
            else:
                input_data = out

                hidden_state, cell_state = self.lstm(
                    input_data, (hidden_state, cell_state)
                )

            out = self.fc(hidden_state)
            out = self.softmax(out)

            outputs[:, count, :] = out
            out = out.argmax(dim=1)

            # Perform one-hot encoding without a loop
            out = to_categorical(out.cpu().numpy(), num_classes=self.output_size)
            out = torch.tensor(out, device=device)

            count += 1

        return outputs    
    
    def train_cell(self, batch_size, source, cell_state, target, epoch):
        # define the output tensor placeholder
        outputs = torch.empty(
            (batch_size, self.MAX_LEN, self.output_size), device=device
        )
        start = torch.zeros((batch_size, self.output_size), device=device)

        # define end of sequence token
        eos = torch.zeros(self.output_size, device=device)
        eos[self.output_size - 1] = 1
        # out = torch.zeros(self.output_size)
        count = 0

        if target is not None:
            # one-hot encode vector [2,2], into [[0,1], [0,1]]
            one_hot_target = self.encode_labels(target)

        while count < self.MAX_LEN:
            if count == 0:
                # start with source and start token
                hidden_state, cell_state = self.lstm(start, (source, cell_state))
                # set count to != 0
            else:
                if epoch is not None and epoch < self.max_epoch:
                    # for small epoch use target as input for faster convergence
                    input_data = one_hot_target[:, count, :]
                else:
                    if target is not None:
                        # determine whether to use teacher forcing or not based on the ratio
                        use_teacher_forcing = (
                            torch.rand(1).item() < self.teacher_force_ratio
                        )
                        # if using teacher forcing, input the real target; otherwise, use predictions

                        # TODO: Stimmt target -> eher one_hot_target, stimmt out?
                        input_data = (
                            one_hot_target[:, count, :] if use_teacher_forcing else out
                        )
                    else:
                        input_data = out

                hidden_state, cell_state = self.lstm(
                    input_data, (hidden_state, cell_state)
                )

            out = self.fc(hidden_state)
            out = self.softmax(out)

            outputs[:, count, :] = out

            # if count == 3:
            #    print(f"Decoder out prob: {out[:4]}")
            #    print(f"Decoder out argmax: {out[:4].argmax(dim = 1)}")
            #    print(f"Decoder output tensor: {outputs[:8, 3, :4]}")

            out = out.argmax(dim=1)

            # Perform one-hot encoding without a loop
            out = to_categorical(out.cpu().numpy(), num_classes=self.output_size)
            out = torch.tensor(out, device=device)

            count += 1

        return outputs
    


class DigiRec(nn.Module):
    def __init__(
        self,
        resnet,
        hidden_size,
        max_len = 10,
        teacher_force_ratio = 0.3,
        max_epoch = 20,
        custom_LSTMCell = False,
    ):
        super(DigiRec, self).__init__()
        self.encoder = Encoder(resnet, hidden_size)
        self.decoder = Decoder(
            hidden_size, teacher_force_ratio, max_len, max_epoch, custom_LSTMCell
        )

    def forward(self, x, target=None, epoch=None):
        x = self.encoder(x)
        x = self.decoder(x, target, epoch)
        return x
