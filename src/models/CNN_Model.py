import torch
import torch.nn as nn

class MultiTaskCNN(nn.Module):
    def __init__(self, hidden_size, num_tasks=10, num_classes=11):
        super(MultiTaskCNN, self).__init__()
        print("Pure CNN Model")
        # Shared Convolutional layers
        self.shared_conv = nn.Sequential(
            nn.Conv2d(3, 64, kernel_size=3, stride=1, padding=1),
            nn.ReLU(),
            nn.MaxPool2d(kernel_size=2, stride=2),
            nn.Conv2d(64, 4, kernel_size=3, stride=1, padding=1),
            nn.ReLU(),
            nn.MaxPool2d(kernel_size=2, stride=2),
            #nn.Conv2d(64, 4, kernel_size=3, stride=1, padding=1),
            #nn.ReLU(),
            #nn.MaxPool2d(kernel_size=2, stride=2)    
        )

        # Task-specific Fully Connected layers
        self.task_fc_layers = nn.ModuleList([
            nn.Sequential(
                nn.Linear(4 * 64 * 64 , hidden_size),
                nn.ReLU(),
                nn.Linear(hidden_size , num_classes),
                nn.Softmax()
            ) for _ in range(num_tasks)
        ])

    def forward(self, x):
        x = self.shared_conv(x)
        #print(x.shape)
        x = x.view(x.size(0), -1)

        # Forward through each task-specific fully connected layer
        task_outputs = [fc_layer(x) for fc_layer in self.task_fc_layers]
        #print(task_outputs[0].shape)
        stacked_tensor = torch.stack(task_outputs, dim=0)
        # Using permute
        stacked_tensor = stacked_tensor.permute(1, 0, 2)
        #print(stacked_tensor.shape)

        return stacked_tensor             
