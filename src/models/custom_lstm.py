import torch
import torch.nn as nn

class CustomLSTM(nn.Module):
 def __init__(self, input_size, hidden_size):
  super(CustomLSTM, self).__init__()
  self.input_size = input_size
  self.hidden_size = hidden_size

  # Initialize LSTM weights and biases
  self.W_i = nn.Parameter(torch.Tensor(input_size, hidden_size * 4))
  self.W_h = nn.Parameter(torch.Tensor(hidden_size, hidden_size * 4))
  self.b = nn.Parameter(torch.Tensor(hidden_size * 4))

  # Initialize forget gate biases to 1
  self.b.data[hidden_size:2 * hidden_size].fill_(1.)

 def forward(self, input, hidden=None):
  batch_size, seq_size, _ = input.size()

  if hidden is None:
   h = torch.zeros(batch_size, self.hidden_size).to(input.device)
   c = torch.zeros(batch_size, self.hidden_size).to(input.device)
  else:
   h, c = hidden

  outputs = []

  for i in range(seq_size):
   gates = input[:, i, :].mm(self.W_i) + h.mm(self.W_h) + self.b
   ingate, forgetgate, cellgate, outgate = gates.chunk(4, 1)

   ingate = torch.sigmoid(ingate)
   forgetgate = torch.sigmoid(forgetgate)
   cellgate = torch.tanh(cellgate)
   outgate = torch.sigmoid(outgate)

   c_t = forgetgate * c + ingate * cellgate
   h_t = outgate * torch.tanh(c_t)

   c = c_t
   h = h_t

   outputs.append(h_t.unsqueeze(1))

  outputs = torch.cat(outputs, dim=1)
  return outputs, (h, c)