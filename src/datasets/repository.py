from torchvision import transforms
from torch.utils.data import random_split

from datasets.cvlhds import CVLHDS
from datasets.multi_mnist import MultiDigitDataset
from datasets.orand import ORAND_Dataset


def get_dataset(name:str, IMAGE_SIZE):
    ## transform for mnist and cvlhds
    transform = transforms.Compose([
            transforms.Resize(IMAGE_SIZE, antialias=True),
            transforms.RandomPerspective(distortion_scale=0.25, p= 0.3),
            transforms.ColorJitter(brightness=0.4, hue= 0.1),
            transforms.GaussianBlur(kernel_size=(9,9)),
        ])
    
    if "cvlhds" in name.lower():
        transform = transforms.Compose([
            transforms.ToTensor(),
            transform
        ])
        cvlhds = CVLHDS(transform = transform)
        # Define the sizes of the training and evaluation sets
        train_size = int(0.9 * len(cvlhds))  # 90% for training
        eval_size = len(cvlhds) - train_size  # 10% for evaluation

        # Use random_split to create training and evaluation datasets
        train_dataset, test_dataset = random_split(cvlhds, [train_size, eval_size])

    elif "mnist" in name.lower():
        # Create an instance of the MultiDigitDataset
        multi_digit_dataset = MultiDigitDataset(transform=transform)
        train_size = int(0.8 * len(multi_digit_dataset))
        test_size = len(multi_digit_dataset) - train_size
        train_dataset, test_dataset = random_split(multi_digit_dataset, [train_size, test_size])

    elif "orand" in name.lower():
        transform = transforms.Compose([
            transforms.Resize(IMAGE_SIZE, antialias=True),
            transforms.ToTensor(),
            transforms.RandomPerspective(distortion_scale=0.25, p= 0.3),
            transforms.ColorJitter(brightness=0.4, hue= 0.1),
            transforms.GaussianBlur(kernel_size=(9,9)),
        ])
        train_dataset = ORAND_Dataset(transform=transform, split="train")
        test_dataset = ORAND_Dataset(transform=transform, split="test")
    else: 
        raise ValueError(f"Dataset '{name}' is not supported")
    
    return train_dataset, test_dataset

