from PIL import Image
from torch.utils.data import Dataset
import pandas as pd

from utils import download_and_extract_tar
from utils import DATA_DIR, label_to_index


class ORAND_Dataset(Dataset):
    mirror = "https://www.orand.cl/orand_car/ORAND-CAR-2014.tar.gz"
    
    def __init__(self, name="ORAND-CAR-2014", split="train", transform=None, seq_length=10):
        """Creates a ORAND_Dataset object

        Args:
            name (str, optional): Name of the dataset. Defaults to "ORAND-CAR-2014".
            split (str, optional): Split of the dataset. Must be either "train" or "test". Defaults to "train".
            transform (function, optional): Transformation to apply to each sample of the dataset. Defaults to None.
        """
        assert split == "train" or split == "test", "Split must be either 'train' or 'test'."

        self.name = name
        self.split = split
        self.root = DATA_DIR / name
        self.transform = transform
        self.seq_length = seq_length

        # download dataset if no local copy is present
        if not self.root.exists():
            self.root.mkdir(parents=True)
            self._download()

        # index to access image files and corresponding labels
        # access the image file paths with 'self.index["image"][<idx>]'
        # access the labels with 'self.index["label"][<idx>]'
        self.index = self._create_index()
        
    def _download(self):
        """Downloads the dataset
        """
        print(f"Downloading {self.name}...")
        download_and_extract_tar(self.mirror, extract_to=DATA_DIR)
        print("Download complete")
    
    def _create_index(self) -> dict:
        """Creates a index to access image file paths and correspondig labels.

        Returns:
            dict: Index containing image file paths and labels
        """
        # read index files from both subsets and merge them to one index
        subsets = ["CAR-A", "CAR-B"]
        subset_prefixes = ["a_", "b_"]
        index_dfs = []
        for ss, ss_prefix in zip(subsets, subset_prefixes):
            # read image file names and labels from index file
            index_file = self.root / ss / f"{ss_prefix}{self.split}_gt.txt"
            df = pd.read_csv(index_file, delimiter="\t", names=("image", "label"), dtype=(str, str))
            
            # add path prefix to image file names
            image_path_prefix = self.root / ss / f"{ss_prefix}{self.split}_images"
            df["image"] = [image_path_prefix / img for img in df["image"]]
            
            index_dfs.append(df)

        # join all subset indexes to one dataset index
        index = pd.concat(index_dfs, ignore_index=True).to_dict()

        return index

    def __len__(self) -> int:
        """Returns the number of samples in this dataset

        Returns:
            int: The number of samples in thes dataset
        """
        return len(self.index['image'])

    def __getitem__(self, idx: int) -> dict:
        """Returns a specific sample of this dataset containing image 'data' and 'label'.

        Args:
            idx (int): Index of the sample

        Raises:
            IndexError: Error out of range

        Returns:
            dict: Sample containing image 'data' and 'label'
        """
        if idx < 0 or idx >= len(self):
            raise IndexError("Index out of range")
        
        # get image
        image_path = self.index["image"][idx]
        image = Image.open(image_path).convert("RGB")

        # apply transformations
        if self.transform is not None:
            image = self.transform(image)

        # get label
        label = self.index["label"][idx]

        # get index encoding
        index_encoding = label_to_index(label, seq_length=self.seq_length)
        
        sample = {'data': image, 'label': label, 'index_encoding': index_encoding}
        return sample
