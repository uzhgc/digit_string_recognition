from torch.utils.data import Dataset
from typing import Optional, Callable
from pathlib import Path
import os
from PIL import Image
import torch
import requests
import zipfile
import io

#from Scripts.utils import download_and_extract_zip
def download_and_extract_zip(url: str, extract_to: str):
    """Downloads a zip file and extracts it to a specified dir

    Args:
        url (str): Source url of the zip file
        extract_to (str, optional): Target dir for the extraction
    """
    # download zip file
    response = requests.get(url)
    
    # raise exception if download was not successful
    response.raise_for_status()

    # extract file
    with zipfile.ZipFile(io.BytesIO(response.content)) as zip_ref:
        zip_ref.extractall(path=extract_to)

class CVLHDS (Dataset):
    mirror_eval = "https://zenodo.org/records/1492173/files/cvl-strings-eval.zip?download=1"
    mirror_train = "https://zenodo.org/records/1492173/files/cvl-strings-train.zip?download=1"


    def __init__(self, root: str = "CVLHDS", transform: Optional[Callable] = None, length: int = 10) -> None:
        """Creates a new CVLHDS object

        Args:
            root (str, optional): The root dir of the dataset. Defaults to "CVLHDS".
            transform (Optional[Callable], optional): Transform performed to each sample. Defaults to None.
        """
        super().__init__()

        self.transform = transform

        # define directories
        self.root = Path(root)
        self.train_dir = self.root / "train"
        self.eval_dir = self.root / "eval"
        self.length = length

        # download data if it does not exist locally
        if not self.root.exists():
            self.root.mkdir()
            self.download()

        # build an image file index
        image_file_index_train = list(sorted(list(self.train_dir.glob("*"))))
        image_file_index_eval = list(sorted(list(self.train_dir.glob("*"))))
        self.image_file_index = image_file_index_train + image_file_index_eval
        
        # count samples
        self.n_train = len(image_file_index_train)
        self.n_eval = len(image_file_index_eval)

            
    def download(self):
        """Downloads the dataset
        """
        print("Downloading dataset...")

        # download train data
        download_and_extract_zip(self.mirror_train, extract_to=self.root)

        # download eval data
        download_and_extract_zip(self.mirror_eval, extract_to=self.root)
        os.rename(self.root / "cvl-strings-eval", self.eval_dir)

        print("Download complete")

    
    def __len__(self) -> int:
        """Returns the number of samples

        Returns:
            int: number of samples
        """     
        return self.n_train + self.n_eval
    

    def __getitem__(self, idx) -> dict:
        """Returns a sample

        Args:
            idx (_type_): The index of the sample

        Returns:
            dict: The sample, containing 'data', 'label' and 'split'
        """
        # get split
        if idx < self.n_train:
            # sample is from train split
            split = "train"
        else:
            # sample is from eval split
            split = "eval"

        # get image
        image_file_path = self.image_file_index[idx]
        image = Image.open(image_file_path)

        # get label
        image_file_name = image_file_path.name
        label = image_file_name.split("-")[0]

        index_encoding = [int(digit) for digit in label] + [10] * (self.length - len(label))
        index_encoding = torch.tensor(index_encoding)
        # apply transform
        if self.transform:
            image = self.transform(image)

        # build sample
        sample = dict(
            data=image, 
            label=label,
            index_encoding=index_encoding, 
            split=split,
        )

        return sample