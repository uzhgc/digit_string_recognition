import torch
from torchvision import datasets, transforms
from torch.utils.data import Dataset
import numpy as np

from utils import DATA_DIR
# Define a custom dataset for multi-digit sequences
class MultiDigitDataset(Dataset):
    def __init__(self, sequence_length_range=(2, 8), transform = None, length = 10):

        # Download MNIST dataset
        transform_mnist = transforms.Compose([
            transforms.ToTensor(),
            transforms.Normalize((0.5,), (0.5,))
        ])
        mnist_dataset = datasets.MNIST(root=DATA_DIR, train=True, download=True, transform=transform_mnist)
        self.mnist_dataset = mnist_dataset

        self.length = length
        self.transform = transform
        self.sequence_length_range = sequence_length_range
        self.sequence_lengths = np.random.randint(
            sequence_length_range[0], sequence_length_range[1] + 1, len(mnist_dataset)
        )

    def __len__(self):
        return len(self.mnist_dataset)

    def __getitem__(self, index):
        sequence_length = self.sequence_lengths[index]
        start_index = np.random.randint(0, len(self.mnist_dataset) - sequence_length + 1)

        images = []
        labels = ""

        for i in range(start_index, start_index + sequence_length):
            image, label = self.mnist_dataset[i]
            images.append(image)
            labels += str(label)
        
        images = torch.cat(images, dim = 2)
        images = torch.cat([images] * 3, dim=0)

        index_encoding = [int(digit) for digit in labels] + [10] * (self.length - len(labels))
        index_encodings = torch.tensor(index_encoding)
        
        label = "".join([str(digit) for digit in labels])

        if self.transform:
            images = self.transform(images)
        sample = {"data": images, "label" : label, "index_encoding": index_encodings}

        return sample