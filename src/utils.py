import requests
import zipfile
import tarfile
import io

import requests
import zipfile
import tarfile
import io
from pathlib import Path
import torch.nn.functional as F
import torch

PROJECT_ROOT = Path(__file__).resolve().parents[2]

DATA_DIR = PROJECT_ROOT / "data"
DATA_DIR.mkdir(exist_ok=True)

class IOStream():
    def __init__(self, path):
        self.f = open(path, 'a')

    def cprint(self, text):
        print(text)
        self.f.write(text+'\n')
        self.f.flush()

    def close(self):
        self.f.close()


def download_and_extract_zip(url: str, extract_to: str):
    """Downloads a zip file and extracts it to a specified dir

    Args:
        url (str): Source url of the zip file
        extract_to (str, optional): Target dir for the extraction
    """
    # download zip file
    response = requests.get(url)
    
    # raise exception if download was not successful
    response.raise_for_status()

    # extract file
    with zipfile.ZipFile(io.BytesIO(response.content)) as zip_ref:
        zip_ref.extractall(path=extract_to)


def download_and_extract_tar(url: str, extract_to: str):
    """Downloads a tar file and extracts it to a specified dir

    Args:
        url (str): Source url of the zip file
        extract_to (str, optional): Target dir for the extraction
    """
    # download tar file
    headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.45 Safari/537.36'
    }
    response = requests.get(url, headers=headers)
    
    # raise exception if download was not successful
    response.raise_for_status()

    # extract file
    with tarfile.open(fileobj=io.BytesIO(response.content)) as tar:
        tar.extractall(path=extract_to)


def label_to_sequence(label, seq_length, idx_blank=10):
  # add blank symbols
  sequence = label.ljust(seq_length, "*")

  # label --> index encoding
  index_encoding = torch.tensor([int(n) if not n == "*" else idx_blank for n in sequence])
  
  # index encoding --> one hot encoding
  sequence = F.one_hot(index_encoding)

  return sequence


def label_to_index(label, seq_length, idx_blank=10):
  # add blank symbols
  sequence = label.ljust(seq_length, "*")

  # label --> index encoding
  index_encoding = torch.tensor([int(n) if not n == "*" else idx_blank for n in sequence])

  return index_encoding


def sequence_to_label(sequence, idx_blank=10, remove_blanks=True):
  # one hot encoding --> index encoding
  index_encoding = torch.argmax(sequence, axis=1)

  # index encoding --> label
  index_list = list(map(str, index_encoding.tolist()))
  index_list = ["*" if n == str(idx_blank) else n for n in index_list]
  label = "".join(index_list)

  # remove blank symbols
  if remove_blanks:
    label = label.replace("*", "")

  return label