import os
from pathlib import Path
import argparse
from datetime import datetime
import random
from typing import List

import torch
from torch.utils.data import DataLoader, random_split
from torch.nn import CTCLoss
from torch.nn.utils.rnn import pad_sequence
import torch.nn.functional as F
from torchaudio.functional import edit_distance
import numpy as np
from pyctcdecode import build_ctcdecoder
from tqdm import tqdm
from torch.utils.tensorboard import SummaryWriter

from utils import IOStream
from models.ctc_model import DigitClassifier
from eval.metrics import soft_accuracy
from datasets import repository

ROOT_DIR = Path(__file__).parents[2]
CHECKPOINT_DIR = ROOT_DIR / "checkpoints"
BACKUP_FILES = [
    "src/ctc_main.py",
    "src/models/ctc_model.py",
]
TRAIN_TEST_SPLIT = [0.8, 0.2]
IMAGE_SIZE_FOR_MODEL = {
    "cnn": (36, 172),
    "resnet": (512, 512)
}

def print_report(io, header, args: dict):
    PADDING = "    "
    report_string = header
    for arg, value in args.items():
        report_string += f"{PADDING}{arg}:{'{:8.4f}'.format(value)}"
    io.cprint(report_string)
    

def normalized_levenshtein_distance(target: str, pred: str) -> float:
    lev_dist = edit_distance(target, pred)
    return min([lev_dist, len(target)]) / len(target)


def avg_normalized_levenshtein_distance(targets: List[str], preds: List[str]):
    assert len(targets) == len(preds), "Target list must have same length as predictions list"
    return np.mean([normalized_levenshtein_distance(t, p) for t, p, in zip(targets, preds)])


def train(args, io, writer):
    # load dataset
    image_size = IMAGE_SIZE_FOR_MODEL[args.encoder_model]
    dataset, _ = repository.get_dataset(args.dataset, image_size)
    dataset_train, dataset_test = random_split(dataset, TRAIN_TEST_SPLIT)

    io.cprint(f"train/test split: {TRAIN_TEST_SPLIT}    train: {len(dataset_train)} samples    test: {len(dataset_test)} samples")

    # load model
    model = DigitClassifier(encoder_model=args.encoder_model).to(device)
    if args.model_path is not None:
        model.load_state_dict(torch.load(args.model_path, map_location=device))

    # setup training
    dataloader_train = DataLoader(dataset_train, batch_size=args.batch_size)
    dataloader_test = DataLoader(dataset_test, batch_size=args.batch_size)
    num_batches_train = len(dataloader_train)
    num_batches_test = len(dataloader_test)

    optimizer = torch.optim.Adam(model.parameters(), lr=args.lr)
    criterion = CTCLoss(blank=10)

    # build ctc decoder
    labels = [str(n) for n in range(10)] + ["_"] # mapping index to label: e.g idx:0 --> label:0, idx:1 --> label:1, ..., idx:10 --> label:_ (blank symbol)
    ctc_decoder = build_ctcdecoder(labels)

    best_test_loss = np.infty
    for epoch in range(args.epochs):
        ######## Training #########
        model.train()
        
        metrics = {}
        metrics["train_loss"] = 0
        metrics["train_time"] = datetime.now()

        for batch in dataloader_train:
            optimizer.zero_grad()
            
            batch_data = batch.get("data").to(device)
            batch_label = batch.get("label")  # list of unpadded strings
            batch_label_tensors = [torch.tensor([int(n) for n in seq]) for seq in batch_label] # list of 1D-tensors with size: [individual sequence length]
            
            targets = pad_sequence(batch_label_tensors, batch_first=True) # size: [batch_size, max_seq_length]
            target_lengths = torch.tensor([len(seq) for seq in batch_label]) # size: [batch_size]

            logits, input_lengths = model(batch_data)
            # logits: size: [emb_width, batch_size, num_classes + 1]
            # input_lengths: size: [batch_size]

            log_probs = F.log_softmax(logits, dim=2)
            loss = criterion(log_probs, targets, input_lengths.to(torch.int32), target_lengths)
            
            loss.backward()
            optimizer.step()
            metrics["train_loss"] += loss.item()
        
        epoch_report_train = f"epoch {epoch} train:"

        # calculate epoch train metrics
        metrics["train_loss"] /= num_batches_train
        metrics["train_time"] = (datetime.now() - metrics["train_time"]).total_seconds()


        ######## Testing ########
        model.eval()

        metrics["test_loss"] = 0
        metrics["test_levenshtein"] = 0
        metrics["test_acc"] = 0
        metrics["test_accSoft"] = 0
        metrics["test_time"] = datetime.now()

        with torch.no_grad():
            for batch in dataloader_test:
                batch_data = batch.get("data").to(device)
                batch_label = batch.get("label")  # list of unpadded strings
                batch_label_tensors = [torch.tensor([int(n) for n in seq]) for seq in batch_label] # list of 1D-tensors with size: [individual sequence length]
                
                targets = pad_sequence(batch_label_tensors, batch_first=True) # size: [batch_size, max_seq_length]
                target_lengths = torch.tensor([len(seq) for seq in batch_label]) # size: [batch_size]

                logits, input_lengths = model(batch_data)
                # logits: size: [emb_width, batch_size, num_classes + 1]
                # input_lengths: size: [batch_size]

                # loss
                log_probs = F.log_softmax(logits, dim=2)
                loss = criterion(log_probs, targets, input_lengths.to(torch.int32), target_lengths)
                metrics["test_loss"] += loss.item()

                # leveshtein distance
                if not args.fast_mode:
                    probs = F.softmax(logits, dim=2)
                    preds = ctc_decoder.decode_batch(
                        logits_list=probs.permute(1, 0, 2).cpu().numpy(),
                        pool=None
                    )
                    metrics["test_levenshtein"] += avg_normalized_levenshtein_distance(batch_label, preds)
                    metrics["test_acc"] += np.mean([1 if t == p else 0 for t, p in zip(batch_label, preds)])
                    metrics["test_accSoft"] += soft_accuracy(batch_label, preds)


            # calculate epoch test metrics
            metrics["test_loss"] /= num_batches_test
            if not args.fast_mode:
                metrics["test_levenshtein"] /= num_batches_test
                metrics["test_acc"] /= num_batches_test
                metrics["test_accSoft"] /= num_batches_test

            metrics["test_time"] = (datetime.now() - metrics["test_time"]).total_seconds()

            # save model if it is better than the current best model
            if metrics["test_loss"] < best_test_loss:
                best_test_loss = metrics["test_loss"]
                torch.save(model.state_dict(), CHECKPOINT_DIR / args.exp_name / 'models' / 'model.pt')
            
        # Write training loss to TensorBoard
        writer.add_scalar('loss/train', metrics["train_loss"], epoch)
        writer.add_scalar('loss/test', metrics["test_loss"], epoch)
        writer.add_scalar('levenshtein/test', metrics["test_levenshtein"], epoch)
        writer.add_scalar('acc/test', metrics["test_acc"], epoch)
        writer.add_scalar('accSoft/test', metrics["test_accSoft"], epoch)
        
        # print train and test metrics
        header = f"Epoch {str(epoch).rjust(3)}:"
        print_report(io, header, args=metrics)
            


def eval(args, io, writer):
    # load dataset
    image_size = IMAGE_SIZE_FOR_MODEL[args.encoder_model]
    _, dataset_eval = repository.get_dataset(args.dataset, image_size)
    io.cprint(f"evaluation dataset: {len(dataset_eval)} samples")
    dataloader_eval = DataLoader(dataset_eval, batch_size=1)
    num_batches = len(dataloader_eval)

    # load model
    model = DigitClassifier(encoder_model=args.encoder_model).to(device)
    model.load_state_dict(torch.load(args.model_path, map_location=device))
    model.eval()
    io.cprint(f"Model loaded from {args.model_path}")

    # build ctc decoder
    labels = [str(n) for n in range(10)] + ["_"] # mapping index to label: e.g idx:0 --> label:0, idx:1 --> label:1, ..., idx:10 --> label:_ (blank symbol)
    ctc_decoder = build_ctcdecoder(labels)

    # metrics
    metrics = dict(levenshtein=0, acc=0, acc10=0, accSoft=0)

    with torch.no_grad():
        for batch in tqdm(dataloader_eval):
            batch_data = batch.get("data").to(device)
            batch_label = batch.get("label")  # list of unpadded strings
            
            logits, _ = model(batch_data)
            probs = F.softmax(logits, dim=2)
            batch_beams = ctc_decoder.decode_beams_batch(
                logits_list=probs.permute(1, 0, 2).cpu().numpy(),
                pool=None
            )
            preds = [out[0][0] for out in batch_beams]

            # get top 10 predictions for each sequence output
            preds_top10 = []
            for seq_beams in batch_beams:
                ps = []
                for beam in seq_beams[:10]:
                    ps.append(beam[0])
                preds_top10.append(ps)

            # calculate metrics
            metrics["levenshtein"] += avg_normalized_levenshtein_distance(batch_label, preds)
            metrics["acc"] += np.mean([1 if t == p else 0 for t, p in zip(batch_label, preds)])
            metrics["acc10"] += np.mean([1 if t in p_top10 else 0 for t, p_top10 in zip(batch_label, preds_top10)])
            metrics["accSoft"] += soft_accuracy(batch_label, preds)
    
    metrics["levenshtein"] /= num_batches # Average normalized levenshtein distance (ANLD)
    metrics["acc"] /= num_batches # Accuracy (ACC)
    metrics["acc10"] /= num_batches # Accuracy for target being in top 10 predictions (ACC10)
    metrics["accSoft"] /= num_batches # Soft accuracy
    
    # print metrics
    header = "Evaluation Result:"
    print_report(io, header, args=metrics)


if __name__ == "__main__":
    timestamp = datetime.now().strftime("%Y.%m.%d_%H:%M:%S")

    # parse command line arguments
    parser = argparse.ArgumentParser(description='Digit String Recognition')
    parser.add_argument('--exp_name', type=str, default=f'exp_{timestamp}', metavar='<exp_name>', help='Name of the experiment (default: "exp_<timestamp>")')
    parser.add_argument('--batch_size', type=int, default=32, metavar='<batch_size>', help='Size of batch (default: 32)')
    parser.add_argument('--epochs', type=int, default=30, metavar='<epochs>', help='Number of epochs to train (default: 100)')
    parser.add_argument('--lr', type=float, default=0.0001, metavar='<lr>', help='Learning rate (default: 0.0001)')
    parser.add_argument('--seed', type=int, default=1, metavar='<seed>', help='Random seed (default: 1)')
    parser.add_argument('--model_path', type=str, metavar='<model_path>', help='Path to model to load for evaluation')
    parser.add_argument('--eval', type=bool,  default=False, action=argparse.BooleanOptionalAction, help='Evaluate the model')
    parser.add_argument('--fast_mode', type=bool, default=False, action=argparse.BooleanOptionalAction, help="Do not calculate Levenshtein distance and Accuracy during training to speedup training")
    parser.add_argument('--encoder_model', type=str, default='resnet', metavar='<encoder_model>', help="Encoder model, must be in ['cnn', 'resnet'] (default: 'cnn')")
    parser.add_argument('--dataset', type=str, default="orand", metavar='<dataset>', help='Dataset to train or evaluate on (default: "orand")')
    args = parser.parse_args()

    # create new checkpoint directory
    CHECKPOINT_DIR.mkdir(exist_ok=True)
    checkpoint = CHECKPOINT_DIR / args.exp_name
    checkpoint.mkdir(exist_ok=True)
    models = checkpoint / "models"
    models.mkdir(exist_ok=True)

    # backup files
    for file_path in BACKUP_FILES:
        file_path = Path(file_path)
        file_name = file_path.name
        dest_file_path = checkpoint / (file_name + ".backup")
        os.system(f"copy {file_path} {dest_file_path}")

    # setup io stream for log file and command line output
    io = IOStream(checkpoint / 'run.log')
    io.cprint(str(args))
    
    # set device
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    io.cprint(f"Using {device}")

    # set seeds for reproducability
    torch.manual_seed(args.seed)
    random.seed(args.seed)
    np.random.seed(args.seed)
    if device == "cuda:0":
        torch.cuda.manual_seed(args.seed)

    # Set up TensorBoard
    writer = SummaryWriter(log_dir=CHECKPOINT_DIR / args.exp_name)

    # start training or testing
    if not args.eval:
        io.cprint("\n____Training____")
        train(args, io, writer)
    else:
        io.cprint("\n____Evaluation____")
        eval(args, io, writer)
