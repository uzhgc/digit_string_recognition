import torch
from sklearn.metrics import confusion_matrix
import matplotlib.pyplot as plt
import seaborn as sns

def conf_matrix(prediction, labels):
    # Flatten the predictions and labels
    prediction = prediction.view(-1)
    labels = labels.view(-1)
    # Define class labels
    class_labels = [str(i) for i in range(11)]

    
    # Calculate the confusion matrix
    conf_matrix = confusion_matrix(labels, prediction, labels=range(11))

    # Display the confusion matrix using seaborn
    plt.figure(figsize=(8, 6))
    sns.heatmap(conf_matrix, annot=True, fmt='d', cmap='Blues', cbar=False,
                xticklabels=class_labels,
                yticklabels=class_labels)
    plt.xlabel('Predicted Label')
    plt.ylabel('True Label')
    plt.title('Confusion Matrix')
    plt.show()


conf_matrix(torch.tensor([[1, 2, 6, 7, 10, 10, 10, 10, 10, 10],[5, 0, 0, 0, 0, 10, 10, 10, 10, 10]]),
        torch.tensor([[1, 2, 6, 7, 10, 10, 10, 10, 10, 10],[5, 0, 0, 0, 8, 10, 10, 10, 10, 10]]))