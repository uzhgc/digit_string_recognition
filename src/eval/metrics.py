import numpy as np
from torchaudio.functional import edit_distance
import torch
from typing import List


def normalized_levenshtein_distance(target: str, pred: str) -> float:
    lev_dist = edit_distance(target, pred)
    return min([lev_dist, len(target)]) / len(target)


def avg_normalized_levenshtein_distance(targets: List[str], preds: List[str]):
    assert len(targets) == len(preds), "Target list must have same length as predictions list"
    return np.mean([normalized_levenshtein_distance(t, p) for t, p, in zip(targets, preds)])


def accuracy(target_batch: torch.Tensor, prediction_batch: torch.Tensor) -> float:
    return np.mean([int(torch.eq(t, p).all()) for t, p in zip(target_batch, prediction_batch)])


def soft_accuracy(target_batch, prediction_batch) -> float:
    if type(target_batch) == list:
        return soft_accuracy_list(target_batch, prediction_batch)
    elif type(target_batch) == torch.Tensor:
        return soft_accuracy_tensor(target_batch, prediction_batch)
    else:
        raise ValueError("Target_batch and prediction_batch must be of type List[str] or type torch.Tensor")


def soft_accuracy_list(target_batch: List[str], prediction_batch: List[str]) -> float:
    num_correct = 0
    num_total = 0
    for targ, pred in zip(target_batch, prediction_batch):
        max_length = max({len(targ), len(pred)})
        num_total += max_length
        num_correct += sum([1 for digit_targ, digit_pred in zip(targ, pred) if digit_targ == digit_pred ])
    return num_correct / num_total


def soft_accuracy_tensor(target_batch: torch.Tensor, prediction_batch: torch.Tensor) -> float:
    return np.mean([sum(torch.eq(t, p))/10 for t, p in zip(target_batch, prediction_batch)])